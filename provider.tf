#provider.tf
provider "aws" {
    region = "ap-southeast-1"
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "terraform"    
}
