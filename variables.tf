
variable "cidr_vpc" {
  description = "CIDR block for the VPC"
  default = "10.0.0.0/16"
}
variable "cidr_subnet_public" {
  description = "CIDR block for the subnet"
  default = "10.0.1.0/24"
}
variable "cidr_subnet_private" {
  description = "CIDR block for the subnet"
  default = "10.0.2.0/24"
}
variable "availability_zone" {
  description = "availability zone to create subnet"
  default = "us-east-2a"
}
variable "public_key_path" {
  description = "Public key path"
  default = "~/.ssh/id_rsa.pub"
}
variable "instance_ami" {
  description = "AMI for aws EC2 instance"
  default = "ami-0cf31d971a3ca20d6"
}
variable "instance_type" {
  description = "type for aws EC2 instance"
  default = "t2.micro"
}
variable "environment_tag" {
  description = "Environment tag"
  default = "Production"
}

variable "amis" {
    description = "AMIs by region"
    default = {
        ap-southeast-1 = "ami-0ccaa193408259c82" # 	ami-ubuntu-18.04-1.13.0-00-1543963388
    }
}

variable "region" {
 description = "AWS region for hosting our your network"
 default = "ap-southeast-1"
}

variable "key_name" {
  description = "Key name for SSHing into EC2"
  default = "ec2-core-app"
}