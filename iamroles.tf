# Cloudwatch logs and S3 role and policy
resource "aws_iam_role" "s3-cloudwatch-role" {
  name               = "s3-cloudwatch-role"
  assume_role_policy = <<-EOF
    {
    "Version": "2012-10-17",
    "Statement": [
    {
        "Action": "sts:AssumeRole",
        "Principal": {
        "Service": "ec2.amazonaws.com"
        },
        "Effect": "Allow",
        "Sid": ""
    }
    ]
    }
    EOF

  tags = {
      tag-key = "tag-value"
  }
}

resource "aws_iam_policy" "s3-clouwatch-allow-all" {
  name        = "s3-clouwatch-allow-all"
  policy      = <<-EOF
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": "s3:*",
                "Resource": "arn:aws:s3:::${aws_s3_bucket.dcore.bucket}/*"
            },{
                "Effect": "Allow",
                "Action": "logs:*",
                "Resource": "*"
            }
        ]
    }
    EOF
}
resource "aws_iam_policy_attachment" "s3-cloudwatch-attachment" {
  name       = "s3-attachment"
  roles      = ["${aws_iam_role.s3-cloudwatch-role.name}"]
  policy_arn = "${aws_iam_policy.s3-clouwatch-allow-all.arn}"
}

resource "aws_iam_instance_profile" "s3-cloudwatch-instance-profile" {
  name  = "s3-instance-profile"
  roles = ["${aws_iam_role.s3-cloudwatch-role.name}"]
}

# # Cloudwatch logs role and policy
# resource "aws_iam_role" "cloudwatch-log-allow" {
#   name               = "cloudwatch-log-allow"
#   assume_role_policy = <<-EOF
#     {
#     "Version": "2012-10-17",
#     "Statement": [
#     {
#         "Action": "sts:AssumeRole",
#         "Principal": {
#         "Service": "ec2.amazonaws.com"
#         },
#         "Effect": "Allow",
#         "Sid": ""
#     }
#         ]
#     }
#     EOF
# }

# resource "aws_iam_policy" "cloudwatch-allow-all" {
#   name        = "cloudwatch-allow-all"
#   description = "cloudwatch-allow-all"
#   policy      = <<-EOF
#     {
#         "Version": "2012-10-17",
#         "Statement": [
#             {
#                 "Effect": "Allow",
#                 "Action": "logs:*",
#                 "Resource": "*"
#             }
#         ]
#     }
#     EOF
# }

# resource "aws_iam_policy_attachment" "cloudwatch-attachment" {
#   name       = "cloudwatch-attachment"
#   roles      = ["${aws_iam_role.cloudwatch-log-allow.name}"]
#   policy_arn = "${aws_iam_policy.cloudwatch-allow-all.arn}"
# }

# resource "aws_iam_instance_profile" "cloudwatch-instance-profile" {
#   name  = "cloudwatch-instance-profile"
#   roles = ["${aws_iam_role.cloudwatch-log-allow.name}"]
# }