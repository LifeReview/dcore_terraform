
resource "aws_vpc" "dcore_vpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "dcore_vpc"
    Location = "Vietnam"
  }
}
resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.dcore_vpc.id}"
  tags = {
    Environment = "${var.environment_tag}"
  }
}

/*
  NAT Gateway
*/
resource "aws_security_group" "nat" {
    name = "vpc_nat"
    description = "Allow traffic to pass from the private subnet to the internet"

    ingress {
        from_port = 40001
        to_port = 40001
        protocol = "tcp"
        cidr_blocks = ["${var.cidr_subnet_private}"]
    }
    ingress {
        from_port = 40000
        to_port = 40000
        protocol = "tcp"
        cidr_blocks = ["${var.cidr_subnet_private}"]
    }
    egress {
        from_port = 40000
        to_port = 40000
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 40000
        to_port = 40000
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    
    vpc_id = "${aws_vpc.dcore_vpc.id}"

    tags = {
        Name = "NATSG"
    }
}

resource "aws_eip" "dcore-eip" {
    vpc = true
}

resource "aws_nat_gateway" "nat_gateway" {
    allocation_id = "${aws_eip.dcore-eip.id}"
    subnet_id = "${aws_subnet.dcore-private.id}"
    depends_on = ["aws_internet_gateway.igw"]
    tags = {
        Name = "Dcore Nat gateway"
    }
}

/*
  Public Subnet
*/
resource "aws_subnet" "dcore_public" {
    vpc_id = "${aws_vpc.dcore_vpc.id}"
    map_public_ip_on_launch = "true"
    cidr_block = "10.0.1.0/24"
    availability_zone = "ap-southeast-1a"

    tags = {
        Name = "Public Subnet"
    }
}

resource "aws_security_group" "dcore_public" {
  name = "dcore_public"
  vpc_id = "${aws_vpc.dcore_vpc.id}"
  ingress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
 egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Environment = "${var.environment_tag}"
  }
}
resource "aws_route_table" "dcore_public" {
    vpc_id = "${aws_vpc.dcore_vpc.id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.igw.id}"
    }

    tags = {
        Name = "Public Subnet"
    }
}
resource "aws_route_table_association" "dcore_public" {
    subnet_id = "${aws_subnet.dcore_public.id}"
    route_table_id = "${aws_route_table.dcore_public.id}"
}

#Bastion host 

resource "aws_instance" "bastion_host" {
    ami = "ami-0ccaa193408259c82"
    instance_type = "t2.micro"
    # subnet
    subnet_id = "${aws_subnet.dcore_public.id}"
    # Security Group
    security_groups = ["${aws_security_group.bastion_sg.id}"]
    # vpc_security_group_ids = ["${aws_security_group.auto_scaling_private.id}"]
    # the Public SSH key
    key_name = "ReviewAppBackEnd"

    user_data = <<-EOF
      #!/bin/bash
      if ! command -v ansible >/dev/null; then
              echo "Installing Ansible dependencies and Git."
              if command -v yum >/dev/null; then
                      sudo yum install -y git python python-devel
              elif command -v apt-get >/dev/null; then
                      sudo apt-get update -qq
                      #sudo apt-get install -y -qq git python-yaml python-paramiko python-jinja2
                      sudo apt-get install -y -qq git python python-dev
              else
                      echo "neither yum nor apt-get found!"
                      exit 1
              fi
              echo "Installing pip via easy_install."
              wget http://peak.telecommunity.com/dist/ez_setup.py
              sudo python ez_setup.py && rm -f ez_setup.py
              sudo easy_install pip
              # Make sure setuptools are installed crrectly.
              sudo pip install setuptools --no-use-wheel --upgrade
              echo "Installing required python modules."
              sudo pip install paramiko pyyaml jinja2 markupsafe
              sudo pip install ansible
              sudo mkdir /etc/ansible
              touch /etc/ansible/hosts              
      fi
    EOF

    tags = {
        Name = "Bastion_host"
    }
}

resource "aws_security_group" "bastion_sg" {
    name = "bastion_sg"
    description = "Allow traffic to pass from the private subnet to the internet"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
      }
    
    vpc_id = "${aws_vpc.dcore_vpc.id}"

    tags = {
        Name = "BASTION_SG"
    }
}

// Sends your public key to the instance
# resource "aws_key_pair" "london-region-key-pair" {
#     key_name = "london-region-key-pair"
#     public_key = "${file(var.PUBLIC_KEY_PATH)}"
# }


/*
  Private Subnet
*/
resource "aws_subnet" "dcore-private" {
    vpc_id = "${aws_vpc.dcore_vpc.id}"
    cidr_block = "${var.cidr_subnet_private}"
    availability_zone = "ap-southeast-1b"

    tags = {
        Name = "Private Subnet"
    }
}

resource "aws_route_table" "dcore-private" {
    vpc_id = "${aws_vpc.dcore_vpc.id}"

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = "${aws_nat_gateway.nat_gateway.id}"
    }

    tags = {
        Name = "Private Subnet"
    }
}

resource "aws_route_table_association" "dcore-private-rta" {
    subnet_id = "${aws_subnet.dcore-private.id}"
    route_table_id = "${aws_route_table.dcore-private.id}"
}

resource "aws_security_group" "dcore_private" {
  name = "dcore_private"
  vpc_id = "${aws_vpc.dcore_vpc.id}"
  ingress {
    from_port = 40000
    to_port = 40000
    protocol = "TCP"
    security_groups = ["${aws_security_group.lb_sg.id}"]
    description = "Allow incoming 40000 from load balancer"
  }

  ingress {
    from_port = 40001
    to_port = 40001
    protocol = "TCP"
    security_groups = ["${aws_security_group.lb_sg.id}"]
    description = "Allow incoming 40000 from load balancer"
  }
  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      security_groups = ["${aws_security_group.bastion_sg.id}"]
      description = "Allow incoming 22 from bastion host"
      # cidr_blocks = ["10.0.1.0/24"]
      # security_groups = ["${aws_security_group.dcore_private}"]
  }
 egress {
    from_port   = 40000
    to_port     = 40000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

   egress {
    from_port   = 40001
    to_port     = 40001
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

  tags = {
    Environment = "${var.environment_tag}"
  }
}

/*
  Load balancer
*/

resource "aws_security_group" "lb_sg" {
  name = "sg_22"
  vpc_id = "${aws_vpc.dcore_vpc.id}"
  ingress {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
 egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Environment = "${var.environment_tag}"
  }
}

resource "aws_lb" "Dcore_Loadbalancer" {
  name               = "DcoreLoadbalancer"
  subnets            = ["${aws_subnet.dcore-private.id}", "${aws_subnet.dcore_public.id}"]
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.lb_sg.id}"]

  tags = {
    Name        = "Dcore_Loadbalancer"
    Environment = "${var.environment_tag}"
  }
}

/*
  Target group
*/

resource "aws_lb_target_group" "lb-TargetGroup" {
  name        = "lb-TargetGroup"
  depends_on  = ["aws_vpc.dcore_vpc"]
  port        = 80
  protocol    = "HTTP"
  vpc_id      = "${aws_vpc.dcore_vpc.id}"
  target_type = "instance"

  health_check {
    interval            = 30
    path                = "/index.html"
    port                = 80
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 5
    protocol            = "HTTP"
    matcher             = "200,202"
  }
}

/*
  Load balancer listener
*/

resource "aws_lb_listener" "dcore-lb-listener" {
  load_balancer_arn = "${aws_lb.Dcore_Loadbalancer.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.lb-TargetGroup.arn}"
    type             = "forward"
  }
}

/*
  Launch configuration
*/
resource "aws_security_group" "auto_scaling_private" {
  name = "auto_scaling_private"
  vpc_id = "${aws_vpc.dcore_vpc.id}"
  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    security_groups = ["${aws_security_group.lb_sg.id}"]
    description = "Allow incoming traffic from load balancer"
  }

  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      security_groups = ["${aws_security_group.bastion_sg.id}"]
      description = "Allow incoming 22 from bastion host"
      # cidr_blocks = ["10.0.1.0/24"]
      # security_groups = ["${aws_security_group.dcore_private}"]
  }
 egress {
    from_port   = 40000
    to_port     = 40000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

   egress {
    from_port   = 40001
    to_port     = 40001
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

  tags = {
    Environment = "${var.environment_tag}"
  }
}

resource "aws_launch_configuration" "dcore_private_lc" {
  image_id               = "${lookup(var.amis,var.region)}"
  instance_type          = "t2.micro"
  security_groups        = ["${aws_security_group.auto_scaling_private.id}"]
  key_name               = "ReviewAppBackEnd"
  iam_instance_profile   = "${aws_iam_instance_profile.s3-cloudwatch-instance-profile.id}"

  user_data = <<-EOF
      #!/bin/bash
      echo y | sudo fuser -vik /var/lib/dpkg/lock-frontend
      sudo apt-get update -y
      sudo apt-get remove docker docker-engine docker.io -y
      sudo apt install docker.io -y
      sudo systemctl start docker
      sudo systemctl enable docker
      sudo usermod -aG docker ubuntu
      docker run --rm --name DCore -d -p 8090:8090 -p 40000:40000  decentnetwork/dcore.ubuntu
    EOF

  lifecycle {
    create_before_destroy = true
  }
}

/*
  Auto Scaling group
*/
resource "aws_autoscaling_group" "dcore_autoscaling_group" {
  launch_configuration = "${aws_launch_configuration.dcore_private_lc.id}"
  vpc_zone_identifier  = ["${aws_subnet.dcore-private.id}"]
  min_size = 2
  max_size = 4
  target_group_arns = ["${aws_lb_target_group.lb-TargetGroup.arn}"]
  health_check_type = "EC2"
  tag {
    key = "Name"
    value = "terraform-dcore"
    propagate_at_launch = true
  }
}


# Cloudwatch CPU alarm over 60%
resource "aws_cloudwatch_metric_alarm" "cpualarm" {
alarm_name = "terraform-alarm"
comparison_operator = "GreaterThanOrEqualToThreshold"
evaluation_periods = "2"
metric_name = "CPUUtilization"
namespace = "AWS/EC2"
period = "120"
statistic = "Average"
threshold = "60"

dimensions = {
  AutoScalingGroupName = "${aws_autoscaling_group.dcore_autoscaling_group.name}"
}

alarm_description = "This metric monitor EC2 instance cpu utilization"
alarm_actions = ["${aws_autoscaling_policy.dcore-scale-up.arn}"]
}

#auto scaling up policy
resource "aws_autoscaling_policy" "dcore-scale-up" {
    name = "dcore-scale-up"
    scaling_adjustment = 1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.dcore_autoscaling_group.name}"
}

# Cloudwatch CPU alarm under 10%
resource "aws_cloudwatch_metric_alarm" "cpualarm-down" {
  alarm_name = "terraform-alarm-down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "10"

dimensions = {
  AutoScalingGroupName = "${aws_autoscaling_group.dcore_autoscaling_group.name}"
}

alarm_description = "This metric monitor EC2 instance cpu utilization"
alarm_actions = ["${aws_autoscaling_policy.dcore-scale-down.arn}"]
}

#auto scale down policy
resource "aws_autoscaling_policy" "dcore-scale-down" {
  name = "dcore-scale-down"
  scaling_adjustment = -1
  adjustment_type = "ChangeInCapacity"
  cooldown = 300
  autoscaling_group_name = "${aws_autoscaling_group.dcore_autoscaling_group.name}"
}


/*
  S3 bucket
*/
resource "aws_s3_bucket" "dcore" {
  bucket = "dcore-s3-terraform-bucket"
  acl = "private"
  depends_on = ["aws_autoscaling_group.dcore_autoscaling_group"]
  versioning {
    enabled = true
  }

  tags = {
    Name = "dcore-s3-terraform-bucket"
  }

}

/*
  Cloudwatch log group
*/
resource "aws_cloudwatch_log_group" "dcore-log-group" {
    depends_on = ["aws_autoscaling_group.dcore_autoscaling_group"]
    name = "dcore-log-group"
    retention_in_days = 0
}

/*
  IAM group policy and IAM group
*/

#admin
resource "aws_iam_group_policy" "dcore_admin_gp" {
  name  = "dcore_admin_gp"
  group = "${aws_iam_group.dcore_admin_gp.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "elasticloadbalancing:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },{
      "Action": [
        "autoscaling:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_group" "dcore_admin_gp" {
  name = "dcore_admin_group"
  path = "/home/admin/"
}

#tester
resource "aws_iam_group_policy" "dcore_tester_gp" {
  name  = "dcore_tester_gp"
  group = "${aws_iam_group.dcore_tester_gp.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_group" "dcore_tester_gp" {
  name = "dcore_tester_group"
  path = "/home/tester/"
}