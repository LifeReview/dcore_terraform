#!/bin/bash
sudo apt-get update
sudo apt-get remove docker docker-engine docker.io -y
sudo apt install docker.io -y
sudo systemctl start docker
sudo systemctl enable docker
usermod -aG docker ubuntu
docker run --rm --name DCore -d -p 8090:8090 -p 40000:40000 decentnetwork/dcore.ubuntu 