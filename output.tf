output "vpc_id" {
  value = "${aws_vpc.dcore_vpc.id}"
}
output "bastion_host" {
  value = "${aws_instance.bastion_host.public_ip}"
}