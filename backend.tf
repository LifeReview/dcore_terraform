terraform {
  backend "s3" {
    region = "ap-southeast-1"
    bucket = "sharetolearn"
    key = "state.tfstate"
    encrypt = true    #AES-256 encryption
  }
}