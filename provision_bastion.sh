#!/bin/bash
ANSIBLE_HOST=52.221.245.194

scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i /home/tuan/win7_share/AWS/ReviewAppKeys/ReviewAppBackEnd.pem /home/tuan/win7_share/AWS/ReviewAppKeys/ReviewAppBackEnd.pem ubuntu@${ANSIBLE_HOST}:/home/ubuntu/.ssh/dcore_key.pem

ssh -i /home/tuan/win7_share/AWS/ReviewAppKeys/ReviewAppBackEnd.pem ubuntu@${ANSIBLE_HOST} 'mkdir /home/ubuntu/ansible'

scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i /home/tuan/win7_share/AWS/ReviewAppKeys/ReviewAppBackEnd.pem ./ansible_config/docker_playbook.yml ubuntu@${ANSIBLE_HOST}:/home/ubuntu/ansible/
scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i /home/tuan/win7_share/AWS/ReviewAppKeys/ReviewAppBackEnd.pem ./ansible_config/install_docker.sh ubuntu@${ANSIBLE_HOST}:/home/ubuntu/ansible/
